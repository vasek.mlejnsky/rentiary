import { Component, OnInit } from '@angular/core';

import { AlertService } from './alert.service';

import { Alert, AlertTypeEnum } from '../alert/alert';
import { AlertTypeEnumAware }   from '../alert/alert-type-enum.decorator';

@Component({
    selector: 'alert',
    templateUrl: 'alert.component.html',
    styleUrls: [ 'alert.component.css' ]
})

@AlertTypeEnumAware // Without this decorator AlertTypeEnum isn't accessible within the template
export class AlertComponent implements OnInit {
    alert: Alert;

    constructor(private alertService: AlertService) { }

    ngOnInit() {
        this.alertService.getAlert().subscribe((alert) => { this.alert = alert; });
    }
}
