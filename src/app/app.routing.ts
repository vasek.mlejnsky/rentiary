import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// TODO: Lazy loading
import { RegisterComponent }    from './register/register.component';
import { LoginComponent }       from './login/login.component';
// END TODO

import { AuthGuard }          from './_guards/auth.guard';
import { LoginRegisterGuard } from './_guards/login-register.guard';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'account', loadChildren: 'app/account/account.module#AccountModule' },
  /* { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent } */

  // Disable lazy loading for login and register because of firebaseui-angular issue:
  // https://github.com/RaphaelJenni/FirebaseUI-Angular/issues/19
  /* { path: 'login',      loadChildren: 'app/login/login.module#LoginModule' },
  { path: 'register',   loadChildren: 'app/register/register.module#RegisterModule' } */

  // TODO: Otherwise route to 404 page
  /* { path: '**', } */
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ],
})
export class AppRouting { }
