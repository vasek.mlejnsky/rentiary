import { Injectable }                                                       from '@angular/core';
import { Router, ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Observable }                                                       from 'rxjs/Observable';

import { UserService } from '../user/user.service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private userService: UserService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.userService.authState$
                .map(auth => {
                    if ((auth === null) || (auth === undefined)) {
                        this.router.navigate(['/login']);
                        return false;
                    }
                    else {
                        return true;
                    }
                });
    }
}