import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccountComponent }     from './account.component';
import { AccountInfoComponent}  from './account-info.component';

import { AuthGuard } from '../_guards/auth.guard';

const accountRoutes: Routes = [
  {
    path: '',
    redirectTo: 'info',
    pathMatch: 'full'
  },
  { path: '',
    component: AccountComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'info', component: AccountInfoComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(accountRoutes)],
  exports: [RouterModule],
})
export class AccountRouting { }
