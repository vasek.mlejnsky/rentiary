import { Injectable } from '@angular/core';

import { Observable }   from 'rxjs';
import { Subject }      from 'rxjs/Subject';

import { Alert } from '../alert/alert';

@Injectable()
export class AlertService {
    private alertSubject = new Subject<Alert>();

    constructor() { }

    alert(alert: Alert): void {
        this.alertSubject.next(alert);
    }

    getAlert(): Observable<Alert> {
        return this.alertSubject.asObservable();
    }
}