import { environment }    from '../../environments/environment';

import { NgModule }     from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }  from '@angular/forms';

import { FirebaseUIModule } from 'firebaseui-angular';
import { AlertModule }      from '../alert/alert.module';

import { LoginRouting } from './login.routing';

import { LoginComponent } from './login.component';

import { UserService }  from '../user/user.service';
import { AlertService } from '../alert/alert.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        LoginRouting,
        AlertModule,
        FirebaseUIModule
    ],
    declarations: [
        LoginComponent
     ],
    exports: [ LoginComponent ],
})
export class LoginModule { }
