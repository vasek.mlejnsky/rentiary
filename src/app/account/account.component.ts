import { Component, OnInit } from '@angular/core';

import { Tabs } from '../_models/tabs.model';

export enum AccountTabsEnum {
    accountInfo,
    listings,
    messages
}

@Component({
    selector: 'account',
    templateUrl: 'account.component.html'
})

export class AccountComponent {
    readonly accountTabTexts: [string] = ['My Info', 'Listings', 'Messages'];
    readonly accountTabIcons: [string] = ['fa-user', 'fa-usd', 'fa-comments-o'];
    readonly accountTabLinks: [string] = ['/account/info', '/account/listings', '/account/messages'];

    tabsModel: Tabs = new Tabs(3, this.accountTabTexts, this.accountTabIcons, this.accountTabLinks);

    constructor() { }

    setActive(index: number): void {
        this.tabsModel.activeTabIndex = index;
    }
}