// Prevents user from visiting "/login" or "/register" if already logged in
import { Injectable }                                                       from '@angular/core';
import { Router, ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Observable }                                                       from 'rxjs/Observable';

import { UserService } from '../user/user.service';

@Injectable()
export class LoginRegisterGuard implements CanActivate {
    constructor(
        private router: Router,
        private userService: UserService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        // From https://stackoverflow.com/questions/44053523/angular-4-w-angularfire-2-and-auth-guard
        return this.userService.authState$
                .map(auth => {
                    if ((auth === null) || (auth === undefined)) {
                        return true;
                    }
                    else {
                        return false;
                    }
                });
    }
}