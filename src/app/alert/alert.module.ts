import { NgModule }     from '@angular/core';
import { CommonModule } from '@angular/common'; // *ngIf, *ngFor, etc

import { AlertComponent }   from './alert.component';
import { AlertService }     from './alert.service';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        AlertComponent
    ],
    exports: [
        AlertComponent // So the Alert directive can be used in other modules
    ]
})
export class AlertModule { }
