export class Tabs {
    private _tabsTotal: number;
    private _activeTabIndex: number = 0;

    private _tabTexts: [string];
    private _tabIcons: [string];
    private _tabLinks: [string];

    get tabsTotal(): number {
        return this._tabsTotal;
    }
    get activeTabIndex(): number {
        return this._activeTabIndex;
    }
    set activeTabIndex(index: number) {
        if (index >= 0 && index <= this._tabsTotal)
            this._activeTabIndex = index;
        else
            this._activeTabIndex = this._tabsTotal;
    }

    constructor(tabsTotal: number, tabTexts?: [string], tabIcons?: [string], tabLinks?: [string]) {
        this._tabsTotal = tabsTotal;

        if (tabTexts.length <= this._tabsTotal) {
            this._tabTexts = tabTexts;
        }
        else {
            throw new RangeError(`Trying to assign a tabTexts array with ${tabTexts.length} items when the total tab count is ${this._tabsTotal}`);
        }

        if (tabIcons.length <= this._tabsTotal) {
            this._tabIcons = tabIcons;
        }
        else {
            throw new RangeError(`Trying to assign a tabIcons array with ${tabIcons.length} items when the total tab count is ${this._tabsTotal}`);
        }

        if (tabLinks.length <= this._tabsTotal) {
            this._tabLinks = tabLinks;
        }
        else {
            throw new RangeError(`Trying to assign a tabLinks array with ${tabLinks.length} items when the total tab count is ${this._tabsTotal}`);
        }
    }

    getTabText(index: number) {
        return this._tabTexts[index];
    }

    getTabIcon(index: number) {
        return this._tabIcons[index];
    }

    getTabLink(index: number) {
        return this._tabLinks[index];
    }

    // Utility method when using NgForOf directive that can iterate only over collections
    createTabsRangeCollection(): number[] {
        let indicesCollection: number[] = [];
        for (let i = 0; i < this._tabsTotal; i++) {
            indicesCollection.push(i);
        }
        return indicesCollection;
    }
}