import { environment }    from '../environments/environment';
import { AppComponent }   from './app.component';

/* First Party Modules */
import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { Router }         from '@angular/router'; // <--- Debug only

/* Third Party Modules */
import { AngularFireModule }      from 'angularfire2';
import { AngularFireAuthModule }  from 'angularfire2/auth';
import { FirebaseUIModule, FirebaseUIService }       from 'firebaseui-angular';


/* Custom Modules */
import { NavbarModule }   from './navbar/navbar.module';
import { HomeModule }     from './home/home.module';
// TODO: Lazy loading
import { LoginModule }  from './login/login.module';
import { RegisterModule }  from './register/register.module';
// END TODO

/* Custom Routing Modules */
import { AccountRouting } from './account/account.routing';
import { AppRouting }     from './app.routing';

/* Custom Services */
import { AlertService } from './alert/alert.service';
import { UserService }  from './user/user.service';

/* Custom Guards */
import { AuthGuard }          from './_guards/auth.guard';
import { LoginRegisterGuard } from './_guards/login-register.guard';

@NgModule({
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    FirebaseUIModule.forRoot(environment.firebaseUiConfig),
    NavbarModule,
    HomeModule,

    LoginModule, // <--- TODO: Lazy loading
    RegisterModule, // <-- TODO: Lazy loading
    AppRouting
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    AlertService,
    UserService,
    AuthGuard,
    LoginRegisterGuard
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
  constructor(router: Router) {
    console.log('Routes: ', JSON.stringify(router.config, undefined, 2)); // <--- Debug only
  }
}
