// Handles current user

import { Injectable }   from '@angular/core';
import { Router }       from '@angular/router';


import { Observable } from 'rxjs/Observable';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';


@Injectable()
export class UserService {
    authState$: Observable<firebase.User>

    constructor(
        private fAuth: AngularFireAuth,
        private router: Router) {
        this.authState$ = fAuth.authState;
        fAuth.authState.subscribe((user) => {
            if (user) {
                console.log('User logged in');
                // Redirect home after login
                // this.router.navigate(['/home']);
            }
            else {
                console.log('User logged out');
                // Redirect home after logout
                // this.router.navigate(['/home']);
            }
        });
     }

    register(username: string, email: string, password: string): firebase.Promise<firebase.User> {
        return this.fAuth.auth.createUserWithEmailAndPassword(email, password)
                .then(user => {
                    if (user) {
                        console.log('User created and logged in');
                        console.log('Updating username...');

                        // To set user's username as displayName we must update user profile after registration
                        this.fAuth.auth.currentUser.updateProfile({
                            displayName: username,
                            photoURL: ''
                        })
                        .then(() => {
                            console.log('Username updated');
                        })
                        .catch((error) => {
                            // TODO: Error handling
                            console.error(`Couldn't update user's username.`, error);
                        });
                    }
                });
    }

    login(email: string, password: string): firebase.Promise<firebase.User> {
        return this.fAuth.auth.signInWithEmailAndPassword(email, password);
    }

    logout(): firebase.Promise<void> {
       return this.fAuth.auth.signOut();
    }
}
